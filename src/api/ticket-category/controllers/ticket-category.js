'use strict';

/**
 *  ticket-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::ticket-category.ticket-category');
