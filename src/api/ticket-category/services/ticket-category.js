'use strict';

/**
 * ticket-category service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::ticket-category.ticket-category');
