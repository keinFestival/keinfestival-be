'use strict';

/**
 * ticket-category router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::ticket-category.ticket-category');
